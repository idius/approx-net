#include "network.hpp"

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <cstring>
#include <cmath>

#include <omp.h>


Network::Network(const std::vector<uint> layer_spec)
{
    std::uniform_real_distribution<float> dist(-0.25, 0.25);

    distribution = dist;

    // add the input and hidden layers

    for (uint n_nodes : layer_spec)
    {
        add_layer(n_nodes);
    }

    // add the output layer

    add_layer(1);

    // create buffer

    n_threads = omp_get_max_threads();

    buffer.resize(n_threads);

    init_buffers();

    // print some information about the network

    uint n_hidden_layers = layers.size() - 2;

    uint n_nodes = 0;

    uint n_weights = 0;

    for (uint i = 0; i < layers.size() - 1; ++i)
    {
        const uint n_nodes_in_this_layer = layers[i].biases.size();
        const uint n_nodes_in_next_layer = layers[i + 1].biases.size();

        n_nodes += n_nodes_in_next_layer;
        n_weights += n_nodes_in_this_layer * n_nodes_in_next_layer;
    }

    n_nodes -= layers.back().biases.size();

    std::cerr << "The network has " << n_hidden_layers << " layers, "
              << n_nodes << " total hidden nodes, and "
              << n_weights << " weights, for a total of "
              << n_weights + n_nodes << " adjustable parameters.\n";
}


float Network::evaluate(const float* input) const
{
    fvector prev_activations(input, input + n_input_nodes);

    float output = 0;

    for (uint l = 1; l < layers.size(); ++l)
    {
        const Layer& lth_layer = layers[l];

        fvector current_activations;

        for (uint node = 0; node < lth_layer.biases.size(); ++node)
        {
            const fvector& node_weights = lth_layer.weights[node];

            const float bias = lth_layer.biases[node];

            float dotprod = 0;

            for (uint link = 0; link < node_weights.size(); ++link)
            {
                dotprod += node_weights[link] * prev_activations[link];
            }

            output = dotprod + bias;

            const float activation = sigmoid(output);

            current_activations.push_back(activation);
        }

        prev_activations.swap(current_activations);
    }

    return output;
}


float Network::evaluate(const float *input, fmatrix *activations) const
{
    if (activations == nullptr)
    {
        std::exit(10);
    }

    float output = 0;

    fvector prev_activations(input, input + n_input_nodes);

    for (uint i = 0; i < n_input_nodes; ++i)
    {
        (*activations)[0][i] = input[i];
    }

    for (uint l = 1; l < layers.size(); ++l)
    {
        const Layer& lth_layer = layers[l];

        fvector current_activations;

        float* const activations_l = (*activations)[l].data();

        for (uint node = 0; node < lth_layer.biases.size(); ++node)
        {
            const fvector& node_weights = lth_layer.weights[node];

            const float bias = lth_layer.biases[node];

            float dotprod = 0;

            for (uint link = 0; link < node_weights.size(); ++link)
            {
                dotprod += node_weights[link] * prev_activations[link];
            }

            output = dotprod + bias;

            const float activation = sigmoid(output);

            activations_l[node] = activation;

            current_activations.push_back(activation);
        }

        prev_activations.swap(current_activations);
    }

    return output;
}



fvector Network::evaluate_set(const fvector &input) const
{
    const uint n_samples = input.size() / n_input_nodes;

    fvector output(n_samples, 0.0f);

#pragma omp parallel for schedule(dynamic)

    for (int i = 0; i < n_samples; ++i)
    {
        const float* sample_values = input.data() + n_input_nodes * i;
        output[i] = evaluate(sample_values);
    }

    return output;
}


void Network::train(const fvector &training_input,
                    const fvector &training_target,
                    const fvector &validation_input,
                    const fvector &validation_target,
                    const uint n_batches,
                    const float learning_rate,
                    const uint max_epochs)
{
    // compute initial score

    fvector network_output = evaluate_set(training_input);

    std::cerr << "Before training, score = " << score(network_output, training_target)
              << " (lower is better).\n";

    std::random_device rd;

    std::mt19937 rand_gen(rd());

    std::uniform_int_distribution<uint> random_index(0, training_target.size() - 1);

    const uint batch_size = training_target.size() / n_batches;

    std::cerr << "Using " << n_batches << " mini-batches per epoch.\n";

    std::cerr << "Each mini-batch contains " << batch_size << " training examples.\n";

    for (uint epoch = 0; epoch < max_epochs; ++epoch)
    {
        // create new mini-batches

        fmatrix input_batches(n_batches);

        fmatrix target_batches(n_batches);

        for (uint batch = 0; batch < n_batches; ++batch)
        {
            input_batches[batch].resize(batch_size * n_input_nodes);
            target_batches[batch].resize(batch_size);

            for (uint i = 0; i < batch_size; ++i)
            {
                const uint rand_idx = random_index(rand_gen);

                for (uint b = 0; b < n_input_nodes; ++b)
                {
                    input_batches[batch][n_input_nodes * i + b] =
                            training_input[n_input_nodes * rand_idx + b];
                }

                target_batches[batch][i] = training_target[rand_idx];
            }
        }

        // update parameters using each mini-batch

        for (uint batch = 0; batch < n_batches; ++batch)
        {
            //std::cerr << "Working on batch " << batch << "\n";

            const fvector& batch_input = input_batches[batch];

            const fvector& batch_target = target_batches[batch];

            learn_from_minibatch(batch_input, batch_target, learning_rate);
        }

        if ((epoch + 1) % 20 == 0)
        {
            network_output = evaluate_set(training_input);

            std::cerr << "After epoch " << epoch + 1 <<", score = "
                      << score(network_output, training_target) << ", validation score = ";

            network_output = evaluate_set(validation_input);

            std::cerr << score(network_output, validation_target) << "\n";
        }

        //char num[6];

        //sprintf(num, "%05d", epoch);

        //std::string filename = "graph_";

        //filename += num;

        //print_dot(filename.c_str());

        //filename = "data_";

        //filename += num;

        //std::ofstream plot_stream(filename.c_str());

        //for (uint i = 0; i < 700; ++i)
        //{
        //    plot_stream << 0.01 * i << "   " << evaluate(0.01 * i) << "\n";
        //}
    }
}

float Network::score(const fvector &network_values, const fvector &training_values)
{
    float sum = 0;

#pragma omp parallel for schedule(dynamic) reduction(+:sum)

    for (uint i = 0; i < network_values.size(); ++i)
    {
        const float diff = network_values[i] - training_values[i];
        sum += diff * diff;
    }

    return 0.5 * sum / network_values.size();
}


void Network::print_dot(const char *filename) const
{
    std::ofstream myfile;

    std::ostream* soutput;

    if (filename)
    {
        myfile.open(filename);

        soutput = &myfile;
    }
    else
    {
        soutput = &std::cout;
    }

    *soutput << "digraph G {\n\n"
                "    splines=line;\n"
                "    size=\"8.5,8.5\";\n"
                "    page=\"11,8.5\";\n"
                "    rankdir=LR;\n"
                "    nodesep=0.33;\n"
                "    ranksep=1.33;\n"
                "    edge [arrowsize=0.4, fontsize=8, arrowhead=\"open\", style=\"setlinewidth(0.5)\"];\n"
                "    node [shape=circle, fontsize=9, width=0.36, fixedsize=true];\n\n";

    // print out all of the nodes

    const std::string base = "    node_";

    const std::string sep = "_";

    const std::string label = " [label=\" \", style=\"filled\", fillcolor=\"";

    const std::string end = "];\n";

    const std::string to = " -> ";

    char alpha_buffer[3];

    for (uint l = 0; l < layers.size() - 1; ++l)
    {
        for (uint node = 0; node < layers[l].biases.size(); ++node)
        {
            const float bias = layers[l].biases[node];

            const std::string hue = (bias > 0) ? "#0000ee" : "#ff0000";

            int alpha = 255 * (0.15 + 0.85 * (2.0f * sigmoid(abs(1.5f * bias)) - 1.0f));

            sprintf(alpha_buffer,"%x", alpha);

            *soutput << base << l << sep << node << label << hue << alpha_buffer << "\"];\n";
        }
    }

    *soutput << base << layers.size() - 1 << sep << "0 [label=\"f(x)\"];\n\n";

    // print the edges

    for (uint l = 0; l < layers.size() - 1; ++l)
    {
        for (uint node = 0; node < layers[l].biases.size(); ++node)
        {
            const std::string start_node = "node_" + std::to_string(l)
                    + sep + std::to_string(node);

            for (uint i = 0; i < layers[l + 1].biases.size(); ++i)
            {
                std::string end_node = "node_" + std::to_string(l + 1)
                        + sep + std::to_string(i);

                const float weight = layers[l + 1].weights[i][node];

                const std::string hue = (weight > 0) ? "#0000ee" : "#ff0000";

                int alpha = 255 * (0.15 + 0.85 * (2.0f * sigmoid(abs(1.5f * weight)) - 1.0f));

                sprintf(alpha_buffer,"%x", alpha);

                *soutput << "    " << start_node << to << end_node //<< ";\n";
                         << " [color=\"" << hue << alpha_buffer << "\"];\n";
            }
        }
    }

    *soutput << "}\n";

    if (filename == nullptr)
    {
        myfile.close();
    }
}


void Network::print_json(const char *filename) const
{
    std::ofstream myfile;

    std::ostream* soutput;

    if (filename)
    {
        myfile.open(filename);

        soutput = &myfile;
    }
    else
    {
        soutput = &std::cout;
    }

    *soutput << "\n**** THE JSON OUTPUT WRITER HAS NOT YET BEEN IMPLEMENTED ****\n";

    *soutput << "}\n";

    if (filename == nullptr)
    {
        myfile.close();
    }
}


void Network::add_layer(uint n_nodes)
{
    // If the network is empty, add an input layer

    if (layers.size() == 0)
    {
        Layer input_layer;

        input_layer.biases.resize(n_input_nodes);

        layers.push_back(input_layer);
    }

    // Add a layer with the specified number of nodes.

    Layer new_layer;

    // First add the biases:

    new_layer.biases.resize(n_nodes, 0);

    // Then add the weights:

    for (uint node = 0; node < n_nodes; ++node)
    {
        // assign random weights

        fvector layer_weights;

        for (uint link = 0; link < layers.back().biases.size(); ++link)
        {
            layer_weights.push_back(distribution(generator));
        }

        new_layer.weights.push_back(layer_weights);
    }

    layers.push_back(new_layer);
}


void Network::learn_from_minibatch(const fvector &batch_input,
                                   const fvector &batch_target,
                                   const float learning_rate)
{
    // reset the buffers

    zero_buffers();

    // compute gradients for each parameter

#pragma omp parallel for schedule(dynamic)

    for (uint i = 0; i < batch_target.size(); ++i)
    {
        const float* input_value = batch_input.data() + n_input_nodes * i;

        const float target_value = batch_target[i];

        const uint thread_id = omp_get_thread_num();

        BackpropBuffer& thread_buffer = buffer[thread_id];

        backprop(input_value, target_value, thread_buffer);
    }

    // combine gradient results from each thread. This can be done more efficiently (partially
    // parallelized), with some effort.

    if (n_threads > 1)
    {
        for (uint t = 1; t < buffer.size(); ++t)
        {
            for (uint l = 1; l < layers.size(); ++l)
            {
                for (uint node = 0; node < layers[l].biases.size(); ++node)
                {
                    buffer[0].deltas[l][node] += buffer[t].deltas[l][node];

                    for (uint prev_node = 0; prev_node < layers[l - 1].biases.size(); ++prev_node)
                    {
                        buffer[0].weight_gradients[l][node][prev_node]
                                += buffer[t].weight_gradients[l][node][prev_node];
                    }
                }
            }
        }
    }

    const fmatrix& bias_grads = buffer[0].deltas;

    const std::vector<fmatrix>& weight_grads = buffer[0].weight_gradients;

    const float rate = learning_rate / float(batch_target.size());

    for (uint l = 1; l < layers.size(); ++l)
    {
        for (uint node = 0; node < layers[l].biases.size(); ++node)
        {
            layers[l].biases[node] -= (rate * bias_grads[l][node]);

            for (uint prev_node = 0; prev_node < layers[l - 1].biases.size(); ++prev_node)
            {
                layers[l].weights[node][prev_node] -= (rate * weight_grads[l][node][prev_node]);
            }
        }
    }
}


void Network::backprop(const float *input_value,
                       const float target_value,
                       Network::BackpropBuffer &buffer) const
{
    // Feedforward to compute activations

    const float output_value = evaluate(input_value, &buffer.activations);

    // compute the output delta.

    buffer.deltas.back()[0] = output_value - target_value;

    // Backpropagate the deltas:

    for (uint l = buffer.deltas.size() - 2; l > 0; --l)
    {
        const fvector& layer_activations = buffer.activations[l];
        fvector& layer_deltas = buffer.deltas[l];
        const fvector& next_layer_deltas = buffer.deltas[l + 1];
        const fmatrix& next_layer_weights = layers[l + 1].weights;

        for (uint node = 0; node < buffer.deltas[l].size(); ++node)
        {
            float delta = 0;

            for (uint n_next = 0; n_next < buffer.deltas[l + 1].size(); ++n_next)
            {
                delta += next_layer_weights[n_next][node] * next_layer_deltas[n_next];
            }

            const float act = layer_activations[node];

            layer_deltas[node] = delta * act * (1 - act);
        }
    }

    // compute the cost derivative with respect to each weight.

    for (uint l = 1; l < buffer.deltas.size(); ++l)
    {
        fmatrix& layer_weight_grads = buffer.weight_gradients[l];
        const fvector& layer_deltas = buffer.deltas[l];
        const fvector& prev_layer_activations = buffer.activations[l - 1];

        for (uint node = 0; node < layers[l].biases.size(); ++node)
        {
            fvector& node_weight_grads = layer_weight_grads[node];
            const float delta = layer_deltas[node];

            for (uint n_prev = 0; n_prev < layers[l - 1].biases.size(); ++n_prev)
            {
                node_weight_grads[n_prev] += delta * prev_layer_activations[n_prev];
            }
        }
    }
}


void Network::init_buffers()
{

#pragma omp parallel for schedule(static)

    for (uint t = 0; t < n_threads; ++t)
    {
        // Initialize the buffers.

        const uint thread_id = omp_get_thread_num();

        BackpropBuffer& thread_buffer = buffer[thread_id];

        const uint n_layers = layers.size();

        thread_buffer.deltas.resize(n_layers);
        thread_buffer.activations.resize(n_layers);
        thread_buffer.weight_gradients.resize(n_layers);

        for (uint l = 0; l < n_layers; ++l)
        {
            const uint n_nodes = layers[l].biases.size();

            thread_buffer.deltas[l].resize(n_nodes, 0.0f);
            thread_buffer.activations[l].resize(n_nodes, 0.0f);
            thread_buffer.weight_gradients[l].resize(n_nodes);
        }

        for (uint l = 1; l < n_layers; ++l)
        {
            const uint n_nodes = layers[l].biases.size();

            const uint n_nodes_prev = layers[l - 1].biases.size();

            for (uint node = 0; node < n_nodes; ++node)
            {
                thread_buffer.weight_gradients[l][node].resize(n_nodes_prev, 0.0f);
            }
        }
    }
}


void Network::zero_buffers()
{
#pragma omp parallel for schedule(static)

    for (uint t = 0; t < n_threads; ++t)
    {
        const uint thread_id = omp_get_thread_num();

        BackpropBuffer& thread_buffer = buffer[thread_id];

        const uint n_layers = layers.size();

        for (uint l = 0; l < n_layers; ++l)
        {
            std::fill(thread_buffer.deltas[l].begin(), thread_buffer.deltas[l].end(), 0.0f);
            std::fill(thread_buffer.activations[l].begin(), thread_buffer.activations[l].end(), 0.0f);
        }

        for (uint l = 1; l < n_layers; ++l)
        {
            const uint n_nodes = layers[l].biases.size();

            for (uint node = 0; node < n_nodes; ++node)
            {
                std::fill(thread_buffer.weight_gradients[l][node].begin(),
                          thread_buffer.weight_gradients[l][node].end(),
                          0.0f);
            }
        }
    }
}
