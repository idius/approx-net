#ifndef NETWORK_H
#define NETWORK_H

#include <vector>
#include <random>

#ifndef N_INPUT_NODES
    #define N_INPUT_NODES 13
#endif

typedef unsigned int uint;
typedef std::vector<float> fvector;
typedef std::vector<fvector>  fmatrix;


/*!
 * \brief The Network class implements an arbitrarily deep, fully-connected neural network which
 * uses stochastic gradient descent + backpropagation for training (i.e., parameter optimization).
 * It assumes that the input to the netowrk is a single floating point number and the output is
 * another floating point number. In other words, the network simply learns a function, f(x).
 */
class Network
{
    /*!
     * \brief The Layer struct contains all parameters for a single layer of a neural network. The
     * node biases and the input weights are stored in a layer.
     */
    struct Layer
    {
        fmatrix weights;
        fvector biases;
    };


    /*!
     * \brief The BackpropBuffer struct is used to store temporary data that is used by the
     * backpropagation algorithm.
     */
    struct BackpropBuffer
    {
        fmatrix deltas; // [layer][node]
        fmatrix activations; // [layer][node]
        std::vector<fmatrix> weight_gradients; // [layer][from-node][to-node]
    };

public:

    static const uint n_input_nodes = N_INPUT_NODES;

    /*!
    * \brief Network is the only constructor for the Network class.
    * \param layer_spec is a vector of positive integers, where each entry specifies the number of
    * nodes in each of the hidden layers. [10, 5, 3] would indicate that the network has three
    * hidden layers with 10, 5, and  3 nodes.
    */
    Network(const std::vector<uint> layer_spec);

    virtual ~Network() {}

    /*!
    * \brief evaluate performs the feed-forward operation on the input value. In other words, it
    * computes the approximation to the function f(x), where x = input.
    * \param input is the independent variable of the function f(x).
    * \param activations (optional) If the activations parameter is provided, the activation for
    * each node (artificial neuron) will be stored in the matrix pointed to by the activations
    * pointer.
    * \return Returns the result of the feed-forward operation (i.e., the approximation of f(x)).
    */
    float evaluate(const float* input) const;

    /*!
     * \brief evaluate is exactly the same as evaluate(const float* input), except that it also
     * stores the activations of each node in the activations matrix.
     * \param input an array of floating point values, which will be interpreted as the values of
     * input nodes.
     * \param activations is a pointer to an fmatrix containing the activation of each node in the
     * network [layer][node].
     * \return Returns the result of the feed-forward operation.
     */
    float evaluate(const float* input, fmatrix* activations) const;

    /*!
    * \brief evaluate is exactly like evaluate(float*), except that it is optimized for performing
    * many feedforward operations at once.
    * \param input is a vector of input values (more precisely, a flattened matrix of values). The
    * size of this vector is Network::n_input_nodes * n_samples, where n_samples is the number of
    * input data samples.
    * \return returns a vector of output values, as if evaluate(float*) was called on each group of
    * of Network::n_input_nodes entries in the input vector.
    * the input vector.
    */
    fvector evaluate_set(const fvector& input) const;

    /*!
    * \brief train starts the network training / learning process (i.e., parameter optimization)
    * using stochastic gradient descent with gradients computed using backpropagation.
    * \param training_input is a vector of independent variables (x values in the approximated
    * function f(x)).
    * \param training_target is a vector containing the exact (desired) values of f(x), where the
    * ith entry in the vector is the output value corresponding to the ith entry of training_input.
    * \param n_batches is the number of mini-batches that will be used in the stochastic gradient
    * descent algorithm.
    * \param learning_rate is the standard learning rate parameter of the network. In this version
    * of the code, the learning rate is constant, rather than adaptive.
    * \param max_epochs is the maximum number of training epochs (i.e., the number of times that
    * the algorithm cycles through complete sets of mini-batches).
    */
    void train(const fvector& training_input,
               const fvector& training_target,
               const fvector& validation_input,
               const fvector& validation_target,
               const uint n_batches = 100,
               const float learning_rate = 0.9, // later, this should be automatically varied
               const uint max_epochs = 100);

    /*!
    * \brief score computes a measure of the quality of the function approximation. This is sometimes
    * also called the cost function. The learning algorithm tries to minimize this quantity.
    * \param network_values is an fvector of approximated values, output from the neural network.
    * \param training_values is a vector of correct (exact) function values.
    * \return Returns the mean squared deviation between the training values and the approximated
    * values. Lower scores indicate better appoximations.
    */
    static float score(const fvector& network_values, const fvector& training_values);

    /*!
    * \brief print_dot prints the network topology and node values as a GraphVis dot file.
    *
    * \param filename is the name of a file in which the dot file will be saved. If not provided,
    * the network will be printed to standard output.
    */
    void print_dot(const char* filename = nullptr) const;


    /*!
    * \brief print_json prints the network topology and node values as a JSON file / object.
    *
    * \param filename is the name of a file in which the dot file will be saved. If not provided,
    * the network will be printed to standard output.
    *
    * \todo implement this.
    */
    void print_json(const char* filename = nullptr) const;

protected:

    /*!
    * \brief add_layer adds a new layer to the neural network
    * \param n_nodes specifies the number of nodes (artificial neurons) that will be placed in the
    * new layer.
    */
    void add_layer(uint n_nodes);

    /*!
    * \brief sigmoid is the sigmoidal neuron activation function (also known as
    * the logistic function).
    */
    inline static const float sigmoid(const float x)
    {
        const float ix = 1 + exp(-x);

        return 1.0f / ix;
    }

    /*!
    * \brief learn_from_minibatch
    * \param batch_input
    * \param batch_target
    * \param learning_rate
    */
    void learn_from_minibatch(const fvector& batch_input,
                              const fvector& batch_target,
                              const float learning_rate);

    /*!
     * \brief backprop computes derivatives with respect to each bias and weight in the network,
     * using backpropagation.
     * \param input_value is a training input value.
     * \param target_value is the training output target value that the network is being asked to
     * learn.
     * \param buffer stores the results of the backpropagation algorithm (i.e., the derivatives of
     * the cost function, with respect to each bias and weight.)
     */
    inline void backprop(const float* input_value,
                         const float target_value,
                         BackpropBuffer& buffer) const __attribute__((hot));

    /*!
     * \brief init_buffers initializes the buffers that will be used to store derivatives and
     * activations for each element of the network.
     */
    void init_buffers();

    /*!
     * \brief zero_buffers re-sets all buffers to zero, while keeping the size of the buffers fixed.
     */
    inline void zero_buffers();

    std::vector<Layer> layers;

    std::default_random_engine generator;

    std::uniform_real_distribution<float> distribution;

    std::vector<BackpropBuffer> buffer;

    uint n_threads;
};


#endif // NETWORK_H
