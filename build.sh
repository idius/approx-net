#! /bin/bash

N_INPUT=13 # the number of nodes in the input layer is a compile-time constant.

FLAGS="-Ofast -fopenmp -std=c++11 -fno-exceptions -fno-rtti -march=native"

g++ -c network.cpp $FLAGS -DN_INPUT_NODES=$N_INPUT
g++ network.o main.cpp -o approx-net $FLAGS -DN_INPUT_NODES=$N_INPUT

strip approx-net

