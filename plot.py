import matplotlib.pylab as plt
import numpy as np
import sys

fname = sys.argv[1]

f = open(fname)

data = np.array([x.split() for x in f]).astype(np.float)

plt.plot(data.T[0],data.T[1])
plt.plot(data.T[0], 2.0 + np.sin(data.T[0]), ':')
plt.xlim([0,6.28])
plt.ylim([0, 3.15])
plt.xlabel("input value (x)")
plt.ylabel("output value [2 + sin(x)]")

plt.savefig("plt_" + fname + ".png")

