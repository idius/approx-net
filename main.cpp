#include "network.hpp"

#include <iostream>


/*!
 * \brief to_bits produces an array of floating point values corresponding to the bits of an integer.
 * \param word is an integer whose bits will be used to construct a floating point array.
 * \param n_bits is the number of bits that will be place into the bits array.
 * \param bits is a floating point array, containing 1.0f for bits in the word that were set and
 * 0.0f for bits in the word that were not set. The order of the bits is inverted in the process.
 * For example, if word = 00000000 00000000 00000000 01001101 and n_bits = 8, the result would be:
 * bits = [1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f].
 */
inline void to_bits(const uint word, const uint n_bits, float* bits)
{
    uint mask = 1;

    for (uint b = 0; b < n_bits; ++b)
    {
        bits[b] = float((word & mask) > 0);
        mask = (mask << 1);
    }
}


class CustomizedNetwork : public Network
{
public:

    using Network::Network;

    float evaluate(const float input) const
    {
        float input_nodes[n_input_nodes];

        to_bits(1000.0f * input, n_input_nodes, input_nodes);

        return Network::evaluate(input_nodes);
    }
};


/*!
 * \brief init_training_data initializes the training data.
 * \param training_input
 * \param training_output
 */
void init_training_data(fvector& training_input, fvector& training_output, const uint n_samples)
{
    std::random_device rd;

    std::mt19937 generator(rd());

    std::uniform_real_distribution<float> distribution(0.0, 6.28);

    const uint n_bits = Network::n_input_nodes;

    training_input.resize(n_samples * n_bits);

    training_output.resize(n_samples);

    float* input = training_input.data();

    for (uint i = 0; i < n_samples; ++i)
    {
        const float random_input = distribution(generator);

        const uint int_representaton = int(1000.0f * random_input);

        to_bits(int_representaton, n_bits, input + i * n_bits);

        training_output[i] = 2.0f + sin(random_input);
    }
}


int main(int argc, char *argv[])
{
    const uint n_layers = argc - 1; // the number of hidden layers

    if (n_layers < 1)
    {
        std::cerr << "Usage: " << argv[0] << " <layer 1 nodes> <layer 2 nodes> "
                                             "... <layer N nodes>\n";
        exit(1);
    }

    std::vector<uint> layer_sizes;

    for (uint layer = 1; layer <= n_layers; layer++)
    {
        layer_sizes.push_back(std::stoi(argv[layer]));
    }

    fvector training_input, training_output, test_input, test_output;

    std::cerr << "Creating the training data...";

    init_training_data(training_input, training_output, 1e5);

    std::cerr << "Creating the validation data...";

    init_training_data(test_input, test_output, 5e4);

    std::cerr << " done.\nCreating the network.\n";

    CustomizedNetwork approx_net(layer_sizes);

    approx_net.train(training_input, training_output,
                     test_input, test_output, 12, 0.18, 1000);

    // output data for plotting

    for (uint i = 0; i < 700; ++i)
    {
        std::cout << 0.01 * i << "   " << approx_net.evaluate(0.01 * i) << "\n";
    }

    //approx_net.print_dot(); // <-- this would output the graph to the standard output stream

    approx_net.print_dot("graph.dot");

    return 0;
}
